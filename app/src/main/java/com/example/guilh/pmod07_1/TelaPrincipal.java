package com.example.guilh.pmod07_1;

import android.content.res.Configuration;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

public class TelaPrincipal extends AppCompatActivity {
    private ListView mDrawerList;
    private ActionBarDrawerToggle mDrawerToggle;
    private DrawerLayout mDrawerLayout;
    private TextView tvStatus;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        tvStatus = (TextView)findViewById(R.id.tvStatus);

        setContentView(R.layout.activity_tela_principal);

        mDrawerList = (ListView)findViewById(R.id.navList);
        mDrawerList.setDivider(null);
        mDrawerLayout = (DrawerLayout)findViewById(R.id.drawer_layout);

        addDrawerItems();
        setupDrawer();

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
    }

    private void addDrawerItems() {
        String[] osArray = {
                "Adicionar contato",
                "Lista de contatos",
                "Buscar contatos",

                "Sair"
        };

        ArrayAdapter<String> mAdapter;
        mAdapter = new ArrayAdapter<>(this, android.R.layout.simple_list_item_1, osArray);
        mDrawerList.setAdapter(mAdapter);

        mDrawerList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            switch (position) {
                case 0:
                    adicionarContato();
                    break;
                case 1:
                    listaContatos();
                    break;
                case 2:
                    buscaContatos();
                    break;
                case 3:
                    sair();
                    break;
                default:
                    Toast.makeText(TelaPrincipal.this, "Item não definido", Toast.LENGTH_SHORT).show();
                    break;
            }

                mDrawerLayout.closeDrawers();
            }
        });
    }

    public void adicionarContato() {
        tvStatus.setText("Adicionar contato");
        Toast.makeText(TelaPrincipal.this, "Adicionar Contato", Toast.LENGTH_SHORT).show();
    }

    public void listaContatos() {
        tvStatus.setText("Lista de contatos");
    }

    public void buscaContatos() {
        tvStatus.setText("Buscar contatos");
        Toast.makeText(TelaPrincipal.this, "Buscar Contatos", Toast.LENGTH_SHORT).show();
    }

    public void sair() {
        Toast.makeText(TelaPrincipal.this, "Sair", Toast.LENGTH_SHORT).show();
    }

    protected void onPostCreate(Bundle savedInstance) {
        super.onPostCreate(savedInstance);
        mDrawerToggle.syncState();
    }

    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        mDrawerToggle.onConfigurationChanged(newConfig);
    }

    private void setupDrawer() {
        mDrawerToggle = new ActionBarDrawerToggle(this, mDrawerLayout, R.string.drawer_open, R.string.drawer_close) {
          public void onDrawerOpened(View drawerView) {
              super.onDrawerOpened(drawerView);
              invalidateOptionsMenu();
          }

          public void onDrawerClosed(View view) {
              super.onDrawerClosed(view);
              invalidateOptionsMenu();
          }
        };

        mDrawerToggle.setDrawerIndicatorEnabled(true);
        mDrawerLayout.setDrawerListener(mDrawerToggle);
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        return mDrawerToggle.onOptionsItemSelected(item) || super.onOptionsItemSelected(item);
    }
}
